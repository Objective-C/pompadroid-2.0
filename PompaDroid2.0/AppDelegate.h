//
//  AppDelegate.h
//  PompaDroid2.0
//
//  Created by John Watson on 6/29/14.
//  Copyright (c) 2014 John Watson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

