//
//  PDAxel.m
//  PompaDroid2.0
//
//  Created by John Watson on 7/27/14.
//  Copyright (c) 2014 John Watson. All rights reserved.
//

#import "PDAxel.h"
#import "sprites.h"

@interface PDAxel ()

@property (strong, nonatomic) SKAction *superAction;
@property (strong, nonatomic) SKAction *idleAction;

@end

@implementation PDAxel

-(instancetype) init
{
    self = [super init];
    
    if( self )
    {
        // custom init
        [self initSprites];
        [self initActions];
    }
    
    return self;
}

-(void) initSprites
{
//    self = [SKSpriteNode spriteNodeWithTexture:SPRITES_TEX_AXEL_IDLE_01];
}

-(void) initActions
{
    self.idleAction = [SKAction animateWithTextures:SPRITES_ANIM_AXEL_IDLE timePerFrame:0.033];
    self.superAction = [SKAction animateWithTextures:SPRITES_ANIM_AXEL_SUPER timePerFrame:0.033];
}

-(void) performSuper
{
    [self runAction:self.superAction];
}

-(void) performIdle
{
    [self runAction:self.idleAction];
}

@end
