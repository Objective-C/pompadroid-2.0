// ---------------------------------------
// Sprite definitions for sprites
// Generated with TexturePacker 3.3.4
//
// http://www.codeandweb.com/texturepacker
// ---------------------------------------

#ifndef __SPRITES_ATLAS__
#define __SPRITES_ATLAS__

// ------------------------
// name of the atlas bundle
// ------------------------
#define SPRITES_ATLAS_NAME @"sprites"

// ------------
// sprite names
// ------------
#define SPRITES_SPR_AXEL_IDLE_01  @"axel/idle/01"
#define SPRITES_SPR_AXEL_IDLE_02  @"axel/idle/02"
#define SPRITES_SPR_AXEL_IDLE_03  @"axel/idle/03"
#define SPRITES_SPR_AXEL_IDLE_04  @"axel/idle/04"
#define SPRITES_SPR_AXEL_IDLE_05  @"axel/idle/05"
#define SPRITES_SPR_AXEL_IDLE_06  @"axel/idle/06"
#define SPRITES_SPR_AXEL_SUPER_01 @"axel/super/01"
#define SPRITES_SPR_AXEL_SUPER_02 @"axel/super/02"
#define SPRITES_SPR_AXEL_SUPER_03 @"axel/super/03"
#define SPRITES_SPR_AXEL_SUPER_04 @"axel/super/04"
#define SPRITES_SPR_AXEL_SUPER_05 @"axel/super/05"
#define SPRITES_SPR_AXEL_SUPER_06 @"axel/super/06"
#define SPRITES_SPR_AXEL_SUPER_07 @"axel/super/07"
#define SPRITES_SPR_AXEL_SUPER_08 @"axel/super/08"

// --------
// textures
// --------
#define SPRITES_TEX_AXEL_IDLE_01  [SKTexture textureWithImageNamed:@"axel/idle/01"]
#define SPRITES_TEX_AXEL_IDLE_02  [SKTexture textureWithImageNamed:@"axel/idle/02"]
#define SPRITES_TEX_AXEL_IDLE_03  [SKTexture textureWithImageNamed:@"axel/idle/03"]
#define SPRITES_TEX_AXEL_IDLE_04  [SKTexture textureWithImageNamed:@"axel/idle/04"]
#define SPRITES_TEX_AXEL_IDLE_05  [SKTexture textureWithImageNamed:@"axel/idle/05"]
#define SPRITES_TEX_AXEL_IDLE_06  [SKTexture textureWithImageNamed:@"axel/idle/06"]
#define SPRITES_TEX_AXEL_SUPER_01 [SKTexture textureWithImageNamed:@"axel/super/01"]
#define SPRITES_TEX_AXEL_SUPER_02 [SKTexture textureWithImageNamed:@"axel/super/02"]
#define SPRITES_TEX_AXEL_SUPER_03 [SKTexture textureWithImageNamed:@"axel/super/03"]
#define SPRITES_TEX_AXEL_SUPER_04 [SKTexture textureWithImageNamed:@"axel/super/04"]
#define SPRITES_TEX_AXEL_SUPER_05 [SKTexture textureWithImageNamed:@"axel/super/05"]
#define SPRITES_TEX_AXEL_SUPER_06 [SKTexture textureWithImageNamed:@"axel/super/06"]
#define SPRITES_TEX_AXEL_SUPER_07 [SKTexture textureWithImageNamed:@"axel/super/07"]
#define SPRITES_TEX_AXEL_SUPER_08 [SKTexture textureWithImageNamed:@"axel/super/08"]

// ----------
// animations
// ----------
#define SPRITES_ANIM_AXEL_IDLE @[ \
        [SKTexture textureWithImageNamed:@"axel/idle/01"], \
        [SKTexture textureWithImageNamed:@"axel/idle/02"], \
        [SKTexture textureWithImageNamed:@"axel/idle/03"], \
        [SKTexture textureWithImageNamed:@"axel/idle/04"], \
        [SKTexture textureWithImageNamed:@"axel/idle/05"], \
        [SKTexture textureWithImageNamed:@"axel/idle/06"]  \
    ]

#define SPRITES_ANIM_AXEL_SUPER @[ \
        [SKTexture textureWithImageNamed:@"axel/super/01"], \
        [SKTexture textureWithImageNamed:@"axel/super/02"], \
        [SKTexture textureWithImageNamed:@"axel/super/03"], \
        [SKTexture textureWithImageNamed:@"axel/super/04"], \
        [SKTexture textureWithImageNamed:@"axel/super/05"], \
        [SKTexture textureWithImageNamed:@"axel/super/06"], \
        [SKTexture textureWithImageNamed:@"axel/super/07"], \
        [SKTexture textureWithImageNamed:@"axel/super/08"]  \
    ]


#endif // __SPRITES_ATLAS__
